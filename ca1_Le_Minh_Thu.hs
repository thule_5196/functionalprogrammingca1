-- ********************** Q1 *******************************************
-- add two number and double result
add_and_double:: (Num a)=> a -> a -> a
add_and_double a b = (a+b)*2

-- ********************** Q2 *******************************************
-- define operator *+

(*+):: (Num a) => a -> a -> a
(*+) a b = a `add_and_double` b

-- alternative: Ln 10
-- (*+) = add_and_double

-- ********************** Q3 *******************************************
-- solve quardric equation using delta= b^2 - 4ac, x= (b +- sqrt(delta))/ 2a

solve_quardric_equation:: Double->Double->Double ->(Double,Double)
solve_quardric_equation a b c = let delta= b^2 - 4*a*c in (((b+delta)/(2*a)),((b+delta)/(2*a)))

-- alternative: Ln 18
-- solve_quardric_equation :: Fractional t => t -> t -> t -> (t, t)

-- ********************** Q4 *******************************************
-- write first_n using take (in-built)

first_n:: Int -> [Int]
first_n a = take a [1..]

-- ********************** Q5 *******************************************
-- write first_n_integers using take_integer

first_n_integers :: Integer -> [Integer]
first_n_integers n
 |n<0 = error "Cant define negative index"
 |otherwise = take_integer n [1..] 
   where
    take_integer _[] = error "Can find in empty list"
    take_integer a (x:xs)
	 | a<0 = error "Wrong Indexing"
	 | a==0 = []
	 | otherwise = x: take_integer (a-1) xs

-- ********************** Q6 *******************************************
-- write double_factorial(n)= f0*f1*f2*..*fn

double_factorial::Integer-> Integer
double_factorial n
 |n<0 =error "can perform factorial on negative number"
 |n==0 = 1
 |otherwise =
  let
    factorial n
	 |n<0 =error "cant perform factorial on negative number"
	 |n==0 =1
	 |otherwise =n * factorial (n-1)
  in factorial n * double_factorial (n-1)
  




